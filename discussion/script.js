// Get post data using fetch()
fetch("https://jsonplaceholder.typicode.com/posts").then((response) => response.json()).then((data) => showPosts(data));

// Add post data.
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	// Prevents the page from loading
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}), // end body: JSON.stringify()
		headers: {"Content-Type": "application/json; charset=UTF-8"}
	}) // end fetch()
	.then((response) => response.json()) // returning response in JSON data.
	.then((data) => {
		console.log(data);
		alert("Successfully added!");

		// Clears the text elements upon post creation
		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
	}); // end then(data)
}); // end document.querySelector().addEventListener()

// Show posts
const showPosts = (posts) => {
	let postEntries = "";
	posts.forEach(post => {
		postEntries += `
			<div id="post-${post.id}" class="my-3">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')" class="btn btn-warning">Edit</button>
				<button onclick="deletePost('${post.id}')" class="btn btn-danger">Delete</button>
			</div>
		`; // end postEntries
	}); // end posts.forEach()
	document.querySelector("#div-post-entries").innerHTML = postEntries
}; // end const showPosts

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	// Removes the disabled attribute from the button.
	document.querySelector("#btn-submit-update").removeAttribute("disabled");
}; // end const editPost

// Update post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	// Prevent page from loading
	e.preventDefault();

	fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}), // end body: JSON.stringify()
		headers: {"Content-Type": "application/json; charset=UTF-8"}
	}) // end fetch()
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert("Successfully updated.");

		document.querySelector("#txt-edit-id").value = null;
		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;
		// Setting back the disabled attribute to true
		document.querySelector("#btn-submit-update").setAttribute("disabled", true);

	}); // end then()
}); // end document.querySelector().addEventListener

const deletePost = (id) => {
	document.querySelector(`#post-${id}`).remove();
};